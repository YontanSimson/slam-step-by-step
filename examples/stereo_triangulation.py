import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import cv2 as cv
import sophus as sp
from skimage.feature import match_descriptors, ORB, plot_matches, CENSURE, BRIEF

from readers.euroc_mav_reader import EurocMavReader
from features.feature_descriptor import FeatureDescriptor
from utils.common import stereo_triangulation_midpoint, mid_point_triangulation_single


if __name__ == "__main__":
    filepath = '/home/yonatan/data/euroc_mav/MH_01_easy/mav0'
    reader = EurocMavReader(filepath)

    timestamp_idx = 23

    cam_timestamps = reader.get_image_timestamps()
    gt_timestamps = reader.get_gt_6dof_timestamps()

    idx = np.where(gt_timestamps[0] > cam_timestamps)[0]
    timestamp = cam_timestamps[timestamp_idx]
    img0 = reader.read_image(timestamp, 0)
    img1 = reader.read_image(timestamp, 1)

    rig_T_world = reader.calc_gt_rig_pose(timestamp)   # world to slam transform
    reader.rig.set_rig_pose(rig_T_world)

    # Source http://dev.ipol.im/~reyotero/bib/bib_all/2008_Agrawal_Konolige__CenSurE_eccv.pdf
    feature_descriptor = FeatureDescriptor()
    kps0, desc0, scale0, orientation0 = feature_descriptor.calculate_descriptor(img0)
    kps1, desc1, scale1, orientation1 = feature_descriptor.calculate_descriptor(img1)

    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))
    ax[0].imshow(img0, cmap=plt.cm.gray)
    ax[0].scatter(kps0[:, 1], kps0[:, 0], 2 ** scale0, facecolors='none', edgecolors='r')
    ax[0].set_title("cam0")

    ax[1].imshow(img1, cmap=plt.cm.gray)
    ax[1].scatter(kps1[:, 1], kps1[:, 0], 2 ** scale1, facecolors='none', edgecolors='r')
    ax[1].set_title("cam1")

    matches12 = match_descriptors(desc0, desc1, cross_check=True, max_ratio=0.9)
    fig, ax = plt.subplots(nrows=1, ncols=1)
    plt.gray()
    plot_matches(ax, img0, img1, kps0, kps1, matches12)
    ax.axis('off')
    ax.set_title("cam0 vs. cam1")

    points0 = kps0[matches12[:, 0], :][:, ::-1].astype(np.float32)
    points1 = kps1[matches12[:, 1], :][:, ::-1].astype(np.float32)

    fig, axes = plt.subplots(1, 2, sharex=True, sharey=True)
    axes[0].imshow(img0, 'gray')
    axes[0].set_title("cam0")
    axes[0].scatter(points0[:, 0], points0[:, 1])
    for idx, pt in enumerate(points0):
        axes[0].text(pt[0] - 5, pt[1], f"{idx}")

    axes[1].imshow(img1, 'gray')
    axes[1].set_title("cam1")
    axes[1].scatter(points1[:, 0], points1[:, 1])
    for idx, pt in enumerate(points1):
        axes[1].text(pt[0] - 5, pt[1], f"{idx}")

    undistorted_p0 = cv.undistortPoints(np.expand_dims(points0, axis=1), reader.rig.cameras[0].K, reader.rig.cameras[0].dist_coeffs)
    undistorted_p1 = cv.undistortPoints(np.expand_dims(points1, axis=1), reader.rig.cameras[1].K, reader.rig.cameras[1].dist_coeffs)

    idx = 33
    ray0 = np.concatenate((undistorted_p0[idx, 0, :], [1]))
    ray0 = ray0 / np.linalg.norm(ray0)
    ray0_rig = reader.rig.cameras[0].R.T @ ray0
    center0 = reader.rig.cameras[0].camera_position_in_world

    ray1 = np.concatenate((undistorted_p1[idx, 0, :], [1]))
    ray1 = ray1 / np.linalg.norm(ray1)
    ray1_rig = reader.rig.cameras[1].R.T @ ray1
    center1 = reader.rig.cameras[1].camera_position_in_world

    p3_mp, r0, r1 = mid_point_triangulation_single(undistorted_p0[idx, 0, :], undistorted_p1[idx, 0, :], reader.rig.cameras)

    lamd = 2
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot([center0[0], center0[0] + lamd * ray0_rig[0]],
            [center0[1], center0[1] + lamd * ray0_rig[1]],
            [center0[2], center0[2] + lamd * ray0_rig[2]], label='cam0 ray')
    ax.plot([center1[0], center1[0] + lamd * ray1_rig[0]],
            [center1[1], center1[1] + lamd * ray1_rig[1]],
            [center1[2], center1[2] + lamd * ray1_rig[2]], label='cam1 ray')
    ax.scatter([center0[0]], [center0[1]], [center0[2]], marker='^', label='cam0 center')
    ax.scatter([center1[0]], [center1[1]], [center1[2]], marker='^', label='cam1 center')
    ax.scatter([p3_mp[0]], [p3_mp[1]], [p3_mp[2]], marker='+', label='mid point')
    ax.legend()

    # https://python.hotexamples.com/examples/cv2/-/triangulatePoints/python-triangulatepoints-function-examples.html
    P0 = reader.rig.projection_matrix_3x4(cam_idx=0)
    P1 = reader.rig.projection_matrix_3x4(cam_idx=1)
    t0 = reader.rig.cam_position_in_world(cam_idx=0)
    t1 = reader.rig.cam_position_in_world(cam_idx=1)

    points_4d = cv.triangulatePoints(P0, P1, undistorted_p0, undistorted_p1)
    points_3d = cv.convertPointsFromHomogeneous(points_4d.T)

    # https://stackoverflow.com/a/40862403/3481173

    points_3d = points_3d.squeeze()
    d1 = P0[:3, :3] @ points_3d.T + P0[-1, :3][:, np.newaxis]
    d2 = P1[:3, :3] @ points_3d.T + P1[-1, :3][:, np.newaxis]

    # Stereo triangulation with midpoint algorithm
    points_3d_mp = stereo_triangulation_midpoint(undistorted_p0, undistorted_p1, reader.rig.cameras)

    # calculate residual
    pts0_2d, residuals0 = reader.rig.cameras[0].project_and_calculate_residual(points_3d_mp, points0)
    pts1_2d, residuals1 = reader.rig.cameras[1].project_and_calculate_residual(points_3d_mp, points1)
    residual = 0.5 * (residuals0 + residuals1)


    print("Done")

