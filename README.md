# SLAM Step by Step

This is intended to be a tutorial for those interested in understanding the basics of SLAM.

The purpose is to show the basic blocks of SLAM in python

## Data

Two popular and accepted datasets are EurocMAV and KITTI 

Download EurocMAV:
```
cd ~/data
mkdir euroc_mav
cd euroc_mav

EUROC_MAV_URL="http://robotics.ethz.ch/~asl-datasets/ijrr_euroc_mav_dataset/"
curl -o MH_01_easy.zip "${EUROC_MAV_URL}machine_hall/MH_01_easy/MH_01_easy.zip"
curl -o MH_02_easy.zip "${EUROC_MAV_URL}machine_hall/MH_02_easy/MH_02_easy.zip"
curl -o MH_03_medium.zip "${EUROC_MAV_URL}machine_hall/MH_03_medium/MH_03_medium.zip"
curl -o MH_04_difficult.zip "${EUROC_MAV_URL}machine_hall/MH_04_difficult/MH_04_difficult.zip"
curl -o MH_05_difficult.zip "${EUROC_MAV_URL}machine_hall/MH_05_difficult/MH_05_difficult.zip"
curl -o V1_01_easy.zip "${EUROC_MAV_URL}vicon_room1/V1_01_easy/V1_01_easy.zip"
curl -o V1_02_medium.zip "${EUROC_MAV_URL}vicon_room1/V1_02_medium/V1_02_medium.zip"
curl -o V1_03_difficult.zip "${EUROC_MAV_URL}vicon_room1/V1_03_difficult/V1_03_difficult.zip"
curl -o V2_01_easy.zip "${EUROC_MAV_URL}vicon_room2/V2_01_easy/V2_01_easy.zip"
curl -o V2_02_medium.zip "${EUROC_MAV_URL}vicon_room2/V2_02_medium/V2_02_medium.zip"
curl -o V2_03_difficult.zip "${EUROC_MAV_URL}vicon_room2/V2_03_difficult/V2_03_difficult.zip"

# unzip files to folders
unzip MH_01_easy.zip -d MH_01_easy
unzip MH_02_easy.zip -d MH_02_easy
unzip MH_03_medium.zip -d MH_03_medium
unzip MH_04_difficult.zip -d MH_04_difficult
unzip MH_05_difficult.zip -d MH_05_difficult
unzip V1_01_easy.zip -d V1_01_easy
unzip V1_02_medium.zip -d V1_02_medium
unzip V1_03_difficult.zip -d V1_03_difficult
unzip V2_01_easy.zip -d V2_01_easy
unzip V2_02_medium.zip -d V2_02_medium
unzip V2_03_difficult.zip -d V2_03_difficult

```

## Installation

Create a virtual environment
Install requirements for installing third party c++ and python packages in Ubuntu 18.04 LTS
```
# clone from source
git clone --recurse-submodules git@bitbucket.org:YontanSimson/slam-step-by-step.git
cd slam-step-by-step

# make virtual environment
sudo apt-get install cmake ccache virtualenv
sudo apt install python3-pip
sudo apt-get install python3-venv
python3 -m venv venv
source ./venv/bin/activate
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
pip install --upgrade pip
pip install -r requirments.txt


# pybind11
PYBIND11_GLOBAL_SDIST=1 python3 -m pip install https://github.com/pybind/pybind11/archive/master.zip

# Sophus
python3 -m pip install sophuspy




```

## Tracking

Stereo Visual Odometry. 
The main reason for choosing Stereo as the core for tracking is to get the correct scale from the visual input. We would like to have a solution that provides 6DOF poses that are correct in scale position and orientation. 