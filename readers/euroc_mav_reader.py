from typing import List
import numpy as np
import cv2 as cv
import pandas as pd
import os
from pyquaternion import Quaternion
from collections import OrderedDict
import sophus as sp
import yaml

from utils.common import interp_se3
from slam.camera import Camera
from slam.rig import Rig


class EurocMavReader(object):
    def __init__(self, data_folder: str):
        self.data_folder = data_folder

        cam0_fn = os.path.join(self.data_folder, 'cam0/sensor.yaml')
        cam0 = Camera(cam0_fn)

        cam1_fn = os.path.join(self.data_folder, 'cam1/sensor.yaml')
        cam1 = Camera(cam1_fn)

        self.num_of_cameras = 2

        self.rig = Rig([cam0, cam1])
        imu_fn = os.path.join(self.data_folder, 'imu0/sensor.yaml')
        with open(imu_fn, 'r') as infile:
            data = yaml.load(infile, Loader=yaml.FullLoader)
        self.T_imu2body = np.array(data['T_BS']['data']).reshape((4, 4))

        self.camera_ts = self.get_image_timestamps()

        imu_data_fn = os.path.join(self.data_folder, 'imu0/data.csv')
        df_imu = pd.read_csv(imu_data_fn, header=[0])
        df_imu.set_index("#timestamp [ns]", inplace=True)

        self.imu_ts = df_imu.index.values

        gt_path = os.path.join(self.data_folder, 'state_groundtruth_estimate0/data.csv')
        df_gt = pd.read_csv(gt_path, header=[0])
        df_gt.set_index("#timestamp", inplace=True)
        # Roll into ordered dict with timeposed SE3
        self.pose_world2body = OrderedDict()

        for idx, row in df_gt.iterrows():
            position = row[[' p_RS_R_x [m]', ' p_RS_R_y [m]', ' p_RS_R_z [m]']].values
            q_wxyz = row[[' q_RS_w []', ' q_RS_x []', ' q_RS_y []', ' q_RS_z []']].values
            R = Quaternion(q_wxyz).rotation_matrix
            pose = sp.SE3(R, position)
            self.pose_world2body[idx] = pose

    def get_image_timestamps(self) -> np.ndarray:
        cam0_ts = np.array([np.uint64(x.rsplit('.')[0]) for x in os.listdir(os.path.join(self.data_folder, 'cam0/data'))])
        cam1_ts = np.array([np.uint64(x.rsplit('.')[0]) for x in os.listdir(os.path.join(self.data_folder, 'cam1/data'))])

        cam_ts = np.intersect1d(cam0_ts, cam1_ts)
        return cam_ts.astype(np.int64)

    def get_gt_6dof_timestamps(self) -> np.ndarray:
        return np.array(list(self.pose_world2body.keys()))

    def get_imu_timestamps(self) -> np.ndarray:
        return self.imu_ts

    def read_image(self, ts: np.uint64, cam_idx: int) -> np.ndarray:
        """

        @param ts:
        @param cam_idx:
        @return:
        """
        image_path = os.path.join(self.data_folder, f"cam{cam_idx}/data/{ts}.png")
        image = cv.imread(image_path, 0)
        return image

    def read_images(self, ts: np.uint64) -> List[np.ndarray]:
        """
        Read images taken at a single timestamp
        @param ts:
        @return:
        """
        images = []
        for cam_idx in range(self.num_of_cameras):
            img = self.read_image(ts, cam_idx)
            images.append(img)

        return images

    def calc_gt_rig_pose(self, timestamp: np.int64):
        """

        """
        gt_timestamps = self.get_gt_6dof_timestamps()
        idx0 = np.where(timestamp - gt_timestamps.astype(np.int64) >= 0)[0][-1]
        idx1 = idx0 + 1
        t = (timestamp - np.int64(gt_timestamps[idx0])) / \
            (np.int64(gt_timestamps[idx1] - np.int64(gt_timestamps[idx0])))

        se3_0 = self.pose_world2body[gt_timestamps[idx0]]
        se3_1 = self.pose_world2body[gt_timestamps[idx1]]

        return interp_se3(se3_0, se3_1, t)
