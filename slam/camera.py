from typing import Tuple
import numpy as np
import cv2 as cv
import os
import yaml
import sophus as sp


class Camera(object):
    def __init__(self, config_yaml: str):
        """
        T_BS: sp.SE3, intrinsis: np.ndarray
        resolution: [752, 480]
        camera_model: pinhole
        intrinsics: [458.654, 457.296, 367.215, 248.375] #fu, fv, cu, cv
        distortion_model: radial-tangential
        distortion_coefficients: [-0.28340811, 0.07395907, 0.00019359, 1.76187114e-05]
        """
        with open(config_yaml, 'r') as infile:
            data = yaml.load(infile, Loader=yaml.FullLoader)
        T_cam2body = np.array(data['T_BS']['data']).reshape((4, 4))

        self.size = np.array(data["resolution"])
        self.pose_cam2rig = sp.SE3(T_cam2body[:3, :3], T_cam2body[:3, -1])

        # fu, fv, cu, cv
        fu, fv, cu, cv = np.array(data["intrinsics"])
        self.K = np.array([[fu, 0, cu], [0, fv, cv], [0, 0, 1]])
        k1, k2, p2, p1 = np.array(data["distortion_coefficients"])
        # distortion_coefficients = k1, k2, p1, p2, k3
        self.dist_coeffs = np.array([k1, k2, p1, p2, 0])
        self.pose_world2rig = None

    def set_pose_world2rig(self, pose: sp.SE3) -> None:
        self.pose_world2rig = pose

    def project(self, p3d: np.ndarray) -> np.ndarray:
        """
        p3d: Nx3 points in meters
        Returns:
            Nx2 features in pixel coordinates
        """
        R = self.R
        t = self.trans
        rvec, _ = cv.Rodrigues(R)
        if len(p3d.shape) == 1:
            projected_pt2d, _ = cv.projectPoints(p3d[np.newaxis, :], rvec, t, self.K, self.dist_coeffs)
        elif len(p3d.shape) == 2:
            rvec, _ = cv.Rodrigues(R)
            projected_pt2d, _ = cv.projectPoints(p3d, rvec, t, self.K, self.dist_coeffs)

        return projected_pt2d.squeeze()

    def project_and_calculate_residual(self, p3d: np.ndarray, p2d: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """

        p3d: Nx3 points in meters
        p2d: Nx2 features in pixel coordinates
        Returns:
            Nx2 p3d points projected into image
            N residuals in pixel coordinates
        """
        p2d_proj = self.project(p3d)
        residuals = np.linalg.norm(p2d - p2d_proj, axis=1)
        return p2d_proj, residuals

    def unproject(self, p2d: np.ndarray):
        """
        p3d: Nx2 feature coordinates in xy pixel units
        Returns:
            features converted into 3D rays in world frame and camera center in wolrd frame
        """
        undistorted = cv.undistortPoints(np.expand_dims(p2d, axis=1),
                                         self.K,
                                         self.dist_coeffs)
        # Convert to homogeneous
        rays = np.concatenate((undistorted, [1]))
        # Normalize
        rays = rays / np.linalg.norm(rays)
        # Orientate ray to world
        rays_world = self.R.T @ rays
        center = self.camera_position_in_world

        return rays_world, center

    def projection_matrix_3x4(self) -> np.ndarray:
        """
        Return a 3x4 numpy array transform from world to cam.Useful for non homogenous points
        """
        return self.projection_matrix().matrix3x4()

    def projection_matrix(self) -> sp.SE3:
        if self.pose_world2rig is None:
            pose_world2cam = self.pose_cam2rig.inverse()
        else:
            pose_world2cam = self.pose_cam2rig.inverse() * self.pose_world2rig
        return pose_world2cam

    @property
    def P(self) -> np.ndarray:
        """
        Projection matrix 3x4
        """
        return self.projection_matrix_3x4()

    @property
    def R(self) -> np.ndarray:
        """
        Translation
        """
        rotation_world2cam = self.pose_cam2rig.rotationMatrix().T @ \
                             self.pose_world2rig.rotationMatrix()
        return rotation_world2cam

    @property
    def trans(self) -> np.ndarray:
        """
        The translation from the projection matrix [R|t] from world to camera
        """
        pose_world2cam = self.projection_matrix()
        return pose_world2cam.translation()

    @property
    def camera_position_in_world(self) -> np.ndarray:
        """
        Position of camera origin in world
        """
        pose_world2cam = self.projection_matrix()
        return pose_world2cam.inverse().translation()

    def rotate_point(self, point: np.ndarray) -> np.ndarray:
        """
        Rotate point or ray from camera to world orientation
        """
        return self.R.T @ point
