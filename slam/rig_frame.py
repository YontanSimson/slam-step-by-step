from typing import List, Optional
import numpy as np

from features.feature_descriptor import FeatureDescriptor

from .frame import Frame
from .rig import Rig


class RigFrame(object):
    def __init__(self, ts: np.uint64, rig: Rig, frames: Optional[List[Frame]] = None, save_image: bool = False):
        self.frames: List[Frame] = frames if frames is not None else []
        self.rig: Rig = rig
        self.ts: np.uint64 = ts  # timestamp in [ns]
        self.save_img: bool = save_image

    def process_images(self, imgs: List[np.ndarray], feature_descriptor: FeatureDescriptor) -> None:
        """
        Calculate features of all the images
        @param feature_descriptor: A function that detects features and calculates their descriptors
        @param imgs:  list of images taken at the same time
        @return:
        """
        for cam_idx, img in enumerate(imgs):
            frame = Frame(save_image=self.save_img)
            frame.process_image(cam_idx, img, feature_descriptor)
            self.frames.append(frame)
