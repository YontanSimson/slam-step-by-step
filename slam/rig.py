from typing import List
import numpy as np
import sophus as sp
from .camera import Camera


class Rig(object):
    def __init__(self, cameras: List[Camera], pose: sp.SE3 = sp.SE3):
        self.cameras = cameras
        self.pose_world2rig = pose

    def projection_matrix(self, cam_idx: int) -> np.ndarray:
        """
        Only the 4x4 transform not the projection from camera world to image pixels
        """
        pose_cam2rig = self.cameras[cam_idx].pose_cam2rig
        pose_world2cam = (pose_cam2rig.inverse() * self.pose_world2rig)
        return pose_world2cam.matrix()

    def projection_matrix_3x4(self, cam_idx: int) -> np.array:
        """
        Only the 3x4 transform not the projection from camera world to image pixels
        """
        return self.projection_matrix(cam_idx)[:3]

    def set_rig_pose(self, pose: sp.SE3) -> None:
        """
        pose: World to slam transformation
        """
        self.pose_world2rig = pose
        for cam in self.cameras:
            cam.set_pose_world2rig(pose)

    def cam_position_in_world(self, cam_idx: int) -> np.ndarray:
        """
        Camera position in world coordinates
        """
        pose_cam2world = self.pose_world2rig.inverse() * self.cameras[cam_idx].pose_cam2rig

        return pose_cam2world.translation()

    @property
    def num_cams(self) -> int:
        return len(self.cameras)
