import numpy as np
from features.feature_descriptor import FeatureDescriptor


class Frame(object):
    def __init__(self, save_image: bool = False):
        self.kps = None
        self.desc = None
        self.scale = None
        self.orientation = None
        self.cam_idx = None  # index of camera
        self.img = None
        self.save_image: bool = save_image

    def process_image(self, cam_idx: int, img: np.ndarray, feature_descriptor: FeatureDescriptor) -> None:
        self.cam_idx = cam_idx
        self.kps, self.desc, self.scale, self.orientation = feature_descriptor.calculate_descriptor(img)
        if self.save_image:
            self.img = img
